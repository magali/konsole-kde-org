---
layout: changelog
title: Konsole 1.2.2 / KDE 3.1.2
sorted: 3012
css-include: /css/main.css
---

+ Don't flicker when selecting entire lines.
+ Crash, selection and sort fixes in schema and session editors.
+ Fixed mouse-wheel in mouse mode.
+ Allow programs to resize windows if enabled.
+ Keep output steady when triple-click selecting.
+ Added "Print" menu command.
