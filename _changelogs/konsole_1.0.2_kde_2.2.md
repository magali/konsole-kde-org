---
layout: changelog
title: Konsole 1.0.2 / KDE 2.2
sorted: 2020
css-include: /css/main.css
---

+ Size of history can be limited to prevent filling up of disk.
+ New keyboard shortcut to start a new session (Ctrl-Alt-n), definable as "newSession" in Keytabs files
+ Control center configuration module with schema editor
