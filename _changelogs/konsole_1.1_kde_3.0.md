---
layout: changelog
title: Konsole 1.1 / KDE 3.0
sorted: 3000
css-include: /css/main.css
---

+ New parameters: <code>--nomenubar</code>, <code>--noframe</code>, <code>--noscrollbar</code> and <code>-tn &lt;foo&gt;</code> (<code>set $TERM=&lt;foo&gt;</code>)
+ Keyboard shortcuts to activate menubar and rename session (Defaults: <kbd>Ctrl-Alt-m</kbd> &amp; <kbd>Ctrl-Alt-s</kbd>).
+ New options: Blinking cursor, configurable line spacing, no/system/visible bell
+ Monitoring for activity and/or silence, sending of input to all sessions (cluster management)
+ History of a session can be cleared, searched and saved to a file.
+ Session types can specify a working directory.
+ Changed behaviour of &quot;New&quot; in toolbar, now starts session of type last selected.
+ Session buttons display state (e.g. bell) and session type icons. Double click renames them.
+ Sessions can be reordered via menu entries or keyboard shortcuts (Default: Ctrl-Shift-Left/Right).
+ Extend selection until end of line if no more characters are printed on that line.
+ Stop scrolling of output when selecting.
+ Drag &amp; drop of selected text (like CDE's dtterm)
+ Pressing Ctrl while pasting with middle mouse button will send selection buffer.
+ Hollow out cursor when losing focus.
+ Support for ScrollLock with LED display.
+ Write utmp entries (requires installed utempter library).
+ Proper implementation of secondary device attributes, MODE_Mouse1000 and wrapped lines.
+ Session management remembers and activates last active session.
+ DCOP interface, sets environment variables KONSOLE_DCOP &amp; KONSOLE_DCOP_SESSION
+ Made embeddable Konsole part configurable.
+ KDE Control Center: Added "Terminal size hint" option and session type editor.
