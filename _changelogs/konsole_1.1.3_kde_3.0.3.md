---
layout: changelog
title: Konsole 1.1.3 / KDE 3.0.3
sorted: 3003
css-include: /css/main.css
---

+ Don't prepend ESC if Meta is pressed if key definition is for "+Alt".
+ Fixed crashes at startup related to broken font installations.
+ Fixed crashes when selecting in history buffer.
