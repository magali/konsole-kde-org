---
layout: changelog
title: Konsole 0.9.8 / KDE 1.1
sorted: 1010
css-include: /css/main.css
---
Initial release.
