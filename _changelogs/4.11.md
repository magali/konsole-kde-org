---
layout: changelog
title: KDE 4.11
sorted: 4110
css-include: /css/main.css
---

Some of the new features and fixes are listed below.  

+ Add "Set Encoding" menu for KonsolePart.
+ Add --fullscreen command-line option.
+ Add support for ANSI SGR escape code 3 (italics).
+ Add code to allow LibKonq to be an optional library.
+ Add code to implement Shift+click to extend selection.
+ Add option to disable ctrl+<mouse-wheel> zooming.
+ Add dialog warning when huge amounts of text is about to be pasted.
+ Add profile option to scroll full/half height via Page Up/Down keys.
