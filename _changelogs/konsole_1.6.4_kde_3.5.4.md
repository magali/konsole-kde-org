---
layout: changelog
title: Konsole 1.6.4 / KDE 3.5.4
sorted: 3054
date: 2006-07-24
css-include: /css/main.css
---

+ Miscellaneous speedups. See SVN commit [546271](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=546271&view=rev).
+ Upon Prev/Next session, only activate new session when session.count > 1. Fixes bug [107197](http://bugs.kde.org/show_bug.cgi?id=107197). See SVN commit [550095](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=550095&view=rev).
+ Add 256 color support. Fixes bug [107487](http://bugs.kde.org/show_bug.cgi?id=107487). See SVN commit [557629](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=557629&view=rev).
+ Add DCOP calls setFont() and font(). Fixes bug [123325](http://bugs.kde.org/show_bug.cgi?id=123325). See SVN commit [549730](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=549730&view=rev).
+ Fix extra character (^L) in konsolepart when using bash vi mode. Fixes bug [127540](http://bugs.kde.org/show_bug.cgi?id=127540). See SVN commit [557931](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=557931&view=rev).
+ Fix the accelerator key for the Config &rarr; Schema tab. Fixes bug [128006](http://bugs.kde.org/show_bug.cgi?id=128006). See SVN commit [545528](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=545528&view=rev).
+ Fix taking 100% cpu on printing tab-completion list in bash. Fixes bug [128488](http://bugs.kde.org/show_bug.cgi?id=128488). See SVN commit [547516](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=547516&view=rev).
+ RMB &rarr; Close Session and tabbar menu &rarr; Close Session now uses the Close Confirmation Dialog. Fixes bug [129514](http://bugs.kde.org/show_bug.cgi?id=129514). See SVN commit [553664](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=553664&view=rev).
+ Reduce flickering when resizing. Fixes bug [54230](http://bugs.kde.org/show_bug.cgi?id=54230). See SVN commit [560010](http://websvn.kde.org/branches/KDE/3.5/kdebase/konsole/?rev=560010&view=rev).
