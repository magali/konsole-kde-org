---
layout: changelog
title: Konsole 1.2.1 / KDE 3.1.1
sorted: 3011
css-include: /css/main.css
---

+ Removed "get prompt back"-hacks, don't assume emacs key shell bindings.
+ Fixed usage of background images with spaces in the filename.
+ Profile support fixes (disabled menubar etc.)
+ Bookmarks invoked from "Terminal Sessions" kicker menu now set correct title.
+ Fixed a problem with the "Linux" font that prevented it from being used with fontconfig.
