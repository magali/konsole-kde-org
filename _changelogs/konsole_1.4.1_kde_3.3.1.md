---
layout: changelog
title: Konsole 1.4.1 / KDE 3.3.1
sorted: 3031
css-include: /css/main.css
---

+ Added AppScreen support to keytab (<a href="http://bugs.kde.org/show_bug.cgi?id=76976">#76976</a>)
+ Cycling trough tabs with keyboard caused terminal size to be printed (<a href="http://bugs.kde.org/show_bug.cgi?id=87274">#87274</a>)
+ Don't crash when showing tabbar with centered background (<a href="http://bugs.kde.org/show_bug.cgi?id=89629">#89629</a>)
