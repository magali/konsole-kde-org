---
layout: changelog
title: Konsole 1.4.2 / KDE 3.3.2
sorted: 3032
css-include: /css/main.css
---

+ Removes checking of sound system for Bell &rarr; System-Notification (<a href="http://bugs.kde.org/show_bug.cgi?id=87664">#87664</a>)
