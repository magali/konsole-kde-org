---
layout: changelog
title: KDE Applications 19.08.0
sorted: 19080
date: 2019-08-15
css-include: /css/main.css
---

+ Restore behavior from 18.12 that profile switching shortcuts opens a new tab.
+ Add headers for each split view and implement Drag & Drop for those headers.  It also implements a toggle button to maximize/restore each view.
+ The Configure Konsole dialog GUI was redesigned according to the HIG.
+ Fix issue that after pressing ALT Konsole ignores all keys due to focus getting stuck in menu when XDG_CURRENT_DESKTOP ≠ kde
+ Remove accelerators from tabs when pressing ALT.
+ Re-enable move tab left / right via keyboard shortcuts.
+ Implement saving sessions recursively which handles the new split views.
+ Implement the new interface of TerminalInterface (v2) for profile handling.  This includes new methods profileProperty, availableProfiles, currentProfileName and setCurrentProfile.
