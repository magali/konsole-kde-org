---
layout: changelog
title: KDE Applications 18.04.0
sorted: 18040
date: 2018-04-19
css-include: /css/main.css
---

To make working on the command line even more enjoyable, Konsole, KDE's terminal emulator application, can now look prettier:

+ You can download color schemes via KNewStuff.
+ The scrollbar blends in better with the active color scheme.
+ By default the tab bar is shown only when needed.

Konsole's contributors did not stop there, and introduced many new features:

+ A new read-only mode and a profile property to toggle copying text as HTML have been added.
+ Under Wayland, Konsole now supports the drag-and-drop menu.
+ Several improvements took place in regards to the ZMODEM protocol: Konsole can now handle the zmodem upload indicator B01, it will show the progress while transferring data, the Cancel button in the dialog now works as it should, and transferring of bigger files is better supported by reading them into memory in 1MB chunks.

Further improvements include:

+ Mouse wheel scrolling with libinput has been fixed, and cycling through shell history when scrolling with the mouse wheel is now prevented.
+ Searches are refreshed after changing the search match regular expression option and when pressing 'Ctrl' + 'Backspace' Konsole will match xterm behaviour.
+ The <code>--background-mode</code> shortcut has been fixed.


