---
layout: changelog
title: Konsole 1.1.3 / KDE 3.0.5a
sorted: 3005
css-include: /css/main.css
---

Don't crash at startup when using Qt 3.1.
