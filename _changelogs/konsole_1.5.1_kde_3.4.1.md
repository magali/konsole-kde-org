---
layout: changelog
title: Konsole 1.5.1 / KDE 3.4.1
sorted: 3041
css-include: /css/main.css
---

+ Allow xterm resize ESC code to work (<a href="http://bugs.kde.org/show_bug.cgi?id=95932">#95932</a>)
+ Fix incorrect schema in detached sessions. (<a href="http://bugs.kde.org/show_bug.cgi?id=98472">#98472</a>)
+ Fix compile errors on amd64 with gcc4 (<a href="http://bugs.kde.org/show_bug.cgi?id=101559">#101559</a>)
+ Expand <code>~</code> in sessions' <code>Exec=</code> (<a href="http://bugs.kde.org/show_bug.cgi?id=102941">#102941</a>)
+ &quot;Monitor for Activity&quot; and &quot;Monitor for Silence&quot; icons are the same. (<a href="http://bugs.kde.org/show_bug.cgi?id=103554">#103554</a>)
